-module(ctr_data_publication_if).
-include("ctr_data.hrl").

-export([
         initialize/1,
         do_run/3
        ]).

-callback init(DataConf :: term()) -> ok.

-type error_reason() :: any().
-type publication() :: #ctr_publication{}.


-callback store_publication(Publication :: publication()) ->
    { ok, Publication :: publication() } | { error, Reason :: error_reason() }.

-callback clean_publications(MaxAge :: integer()) ->
    ok.

initialize(DataConf) ->
    Module = ctr_data_conf:pub_mod(DataConf),
    lager:info("data publication interface is ~p", [Module]),
    Module:init(DataConf).

do_run(Function, Arguments, DataConf) ->
    Module = ctr_data_conf:pub_mod(DataConf),
    apply(Module, Function, Arguments).

