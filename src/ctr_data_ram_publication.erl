-module(ctr_data_ram_publication).

-behaviour(ctr_data_publication_if).
-include("ctr_data.hrl").

-export([
         store_publication/1,
         init/1,
         clean_publications/1,

         dump_all/0
         ]).

init(DataConf) ->
    create_table(DataConf).


store_publication(Pub) ->
    #ctr_publication{
       realm = Realm,
       topic = Topic
      } = Pub,
    NewPubId = ctr_utils:gen_global_id(),
    Now = erlang:system_time(second),
    NewPub = Pub#ctr_publication{id = NewPubId, ts=Now},
    CollectSubs =
        fun(#ctr_subscription{id = SubId, match=Match,
                              subscribers = Subs}, AllSubs) ->
                [ #{id=>SubId, match=>Match,  subs=>Subs}  | AllSubs ]
        end,

    LookupAndStore =
        fun() ->
                case mnesia:wread({ctr_publication, NewPubId}) of
                    [] ->
                        {ok, Found} = ctr_data:match_subscription(Topic, Realm),
                        AllSubs = lists:foldl(CollectSubs, [], Found),
                        UpdatedPub = NewPub#ctr_publication{subs = AllSubs},
                        ok = mnesia:write(UpdatedPub),
                        {ok, UpdatedPub};
                    _ ->
                        {error, pub_id_exists}
                end
        end,
    Result = mnesia:transaction(LookupAndStore),
    handle_publication_store_result(Result, Pub).


handle_publication_store_result({atomic, {ok, Publication}}, _Pub0) ->
    {ok, Publication};
handle_publication_store_result({atomic, {error, pub_id_exists}}, Pub0) ->
    store_publication(Pub0).

remove_publication(Id) ->
    DeletePublication =
        fun() ->
                mnesia:delete({ctr_publication, Id})
        end,
    Result = mnesia:transaction(DeletePublication),
    handle_publication_remove_result(Result).

handle_publication_remove_result({atomic, ok}) ->
    ok;
handle_publication_remove_result(Error) ->
    {error, Error}.

clean_publications(MaxAge) ->
    Now = erlang:system_time(second),
    MaxTimeStamp = Now - MaxAge,
    MatchHead = #ctr_publication{id='$1', ts='$2', _='_'},
    Guard = {'<', '$2', MaxTimeStamp},
    Result = '$1',
    FindIds =
        fun() ->
            mnesia:select(ctr_publication, [{MatchHead, [Guard], [Result]}])
        end,
    MnesiaResult = mnesia:transaction(FindIds),
    handle_find_ids_result(MnesiaResult).



handle_find_ids_result({atomic, []}) ->
    ok;
handle_find_ids_result({atomic, ListOfIds}) ->
    lager:debug("deleting publications ~p", [ListOfIds]),
    Delete =
        fun(Id, _) ->
            remove_publication(Id)
        end,
    lists:foldl(Delete, ok, ListOfIds),
    ok;
handle_find_ids_result(Error) ->
    lager:error("deleting of publications caused error: ~p", [Error]),
    ok.


dump_all() ->
    Print = fun(Entry, _) ->
                    lager:debug("~p", [Entry]),
                    ok
            end,
    Transaction = fun() ->
                          lager:debug("*** publications ***"),
                          mnesia:foldl(Print, ok, ctr_publication),
                          lager:debug("*** end ***"),
                          ok
                  end,
    mnesia:transaction(Transaction).



create_table(DataConf) ->
    MnesiaDir = ctr_data_conf:mnesia_dir(DataConf),
    ct_data_util:create_mnesia_schema_if_needed(MnesiaDir),
    mnesia:delete_table(ctr_publication),
    PubDef = [{attributes, record_info(fields, ctr_publication)},
              {ram_copies, [node()]},
              {index, [realm, topic]}
             ],
    {atomic, ok} = mnesia:create_table(ctr_publication, PubDef),
    ok.
