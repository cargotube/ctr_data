-module(ctr_data_sup).

-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

-spec start_link() -> {ok, pid()}.
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, noparams).

init(noparams) ->
    Procs = [
             clean_worker()
            ],
    Flags = #{},
    {ok, {Flags, Procs}}.



clean_worker() ->
    #{ id => cleaner,
       start => {ctr_data_cleaner, start_link, []}
     }.
