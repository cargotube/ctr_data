-module(ctr_data_invocation_if).
-include("ctr_data.hrl").

-export([
         initialize/1,
         do_run/3
        ]).

-callback init(DataConf :: term()) -> ok.


-type uri() :: binary().
-type error_reason() :: any().
-type id() :: non_neg_integer().
-type invocation() :: #ctrd_invocation{}.


-callback add_invocation( Invocation :: invocation() ) ->
    {ok, UpdatedInvocation :: invocation()} | {error, Reason :: any()}.

-callback invocation_add_result( Result :: any(), InvocationId :: id(),
                                 Realm :: uri() ) ->
    ok | {error, not_found}.

-callback invocation_set_state( State :: atom(), InvocationId :: id(),
                                 Realm :: uri() ) ->
    ok | {error, not_found}.

-callback get_invocation_by_callee(InvocationId :: id(), Realm :: uri()) ->
    {ok, Invocation :: invocation()} | {error, Reason :: error_reason()}.

-callback get_invocation_by_caller(CallerSessId :: id(), ReqId :: id(),
                                Realm :: uri()) ->
    {ok, Invocation :: invocation()} | {error, Reason :: error_reason()}.

-callback remove_invocation(InvocationId :: id(), Realm :: uri()) ->
    ok | {error, Reason :: error_reason()}.

-callback clean_invocations(MaxAge :: integer()) ->
    ok.


initialize(DataConf) ->
    Module = ctr_data_conf:inv_mod(DataConf),
    lager:info("data invocation interface is ~p", [Module]),
    Module:init(DataConf).

do_run(Function, Arguments, DataConf) ->
    Module = ctr_data_conf:inv_mod(DataConf),
    apply(Module, Function, Arguments).

