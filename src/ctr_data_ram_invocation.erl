-module(ctr_data_ram_invocation).
-behaviour(ctr_data_invocation_if).

-include("ctr_data.hrl").

-export([
         add_invocation/1,
         invocation_add_result/3,
         invocation_set_state/3,
         get_invocation_by_callee/2,
         get_invocation_by_caller/3,
         remove_invocation/2,
         clean_invocations/1,

         dump_all/0,
         init/1
        ]).


init(DataConf) ->
    create_table(DataConf).


add_invocation(Invoc) ->
    NewId = ctr_utils:gen_global_id(),
    Now = erlang:system_time(second),
    NewInvoc = Invoc#ctrd_invocation{id = NewId, ts = Now},
    StoreInvocation =
        fun() ->
                case mnesia:wread({ctrd_invocation, NewId}) of
                    [] ->
                        ok = mnesia:write(NewInvoc),
                        {ok, NewInvoc};
                    _ ->
                        {error, id_exists}
                end
        end,
    Result = mnesia:transaction(StoreInvocation),
    handle_invocation_store_result(Result, NewInvoc).


handle_invocation_store_result({atomic, {ok, Invoc}}, _) ->
    {ok, Invoc};
handle_invocation_store_result({atomic, {error, id_exists}}, Invoc) ->
    add_invocation(Invoc).

invocation_add_result(Result, InvocationId, _Realm) ->
    FindInvocation =
        fun() ->
                case mnesia:wread({ctrd_invocation, InvocationId}) of
                    [#ctrd_invocation{results = Results } = Invoc] ->
                        NewResults = Results ++ [Result],
                        NewInvoc = Invoc#ctrd_invocation{results = NewResults},
                        ok = mnesia:write(NewInvoc),
                        ok;
                    _ -> {error, not_found}
                end
        end,
    AddResult = mnesia:transaction(FindInvocation),
    handle_add_result_result(AddResult).

handle_add_result_result({atomic, ok}) ->
    ok;
handle_add_result_result({atomic, {error, Reason}}) ->
    {error, Reason}.

invocation_set_state(State, InvocationId, _Realm) ->
    FindInvocation =
        fun() ->
                case mnesia:wread({ctrd_invocation, InvocationId}) of
                    [#ctrd_invocation{} = Invoc] ->
                        NewInvoc = Invoc#ctrd_invocation{state = State},
                        ok = mnesia:write(NewInvoc),
                        ok;
                    _ -> {error, not_found}
                end
        end,
    SetState = mnesia:transaction(FindInvocation),
    handle_set_state_result(SetState).


handle_set_state_result({atomic, ok}) ->
    ok;
handle_set_state_result({atomic, {error, Reason}}) ->
    {error, Reason}.


get_invocation_by_callee(InvocationId, _Realm) ->
    FindInvocation =
        fun() ->
                case mnesia:read({ctrd_invocation, InvocationId}) of
                    [Invoc] -> {ok, Invoc};
                    _ -> {error, not_found}
                end
        end,
    Result = mnesia:transaction(FindInvocation),
    handle_invocation_find_result(Result).


handle_invocation_find_result({atomic, {ok, Invocation}}) ->
    {ok, Invocation};
handle_invocation_find_result(_) ->
    {error, not_found}.


get_invocation_by_caller(CallerSessId, ReqId, _Realm) ->
    FindInvocation =
        fun() ->
                case mnesia:index_read(ctrd_invocation, CallerSessId,
                                       #ctrd_invocation.caller_sess_id) of
                    Invocs when is_list(Invocs) -> Invocs;
                    _ -> []
                end
        end,
    Result = mnesia:transaction(FindInvocation),
    handle_invocation_find_caller_result(Result, CallerSessId, ReqId).


handle_invocation_find_caller_result({atomic, Invocations}, SessId, ReqId) ->
    Filter =
      fun(#ctrd_invocation{caller_sess_id=S, caller_req_id=R}) ->
        (ReqId == R) and (SessId == S)
      end,
    Filtered = lists:filter(Filter, Invocations),
    handle_invocation_find_caller_result(Filtered, SessId, ReqId);
handle_invocation_find_caller_result([Invocation], _SessId, _ReqId) ->
    {ok, Invocation};
handle_invocation_find_caller_result(_, _, _) ->
    {error, not_found}.




remove_invocation(Id, _Realm) ->
    DeleteInvocation =
        fun() ->
                mnesia:delete({ctrd_invocation, Id})
        end,
    Result = mnesia:transaction(DeleteInvocation),
    handle_invocation_remove_result(Result).

handle_invocation_remove_result({atomic, ok}) ->
    ok;
handle_invocation_remove_result(Error) ->
    {error, Error}.

clean_invocations(MaxAge) ->
    Now = erlang:system_time(second),
    MaxTimeStamp = Now - MaxAge,
    MatchHead = #ctrd_invocation{id='$1', ts='$2', _='_'},
    Guard = {'<', '$2', MaxTimeStamp},
    Result = '$1',
    FindIds =
        fun() ->
            mnesia:select(ctrd_invocation, [{MatchHead, [Guard], [Result]}])
        end,
    MnesiaResult = mnesia:transaction(FindIds),
    handle_find_ids_result(MnesiaResult).


handle_find_ids_result({atomic, []}) ->
    ok;
handle_find_ids_result({atomic, ListOfIds}) ->
    lager:debug("deleting invocations ~p", [ListOfIds]),
    Delete =
        fun(Id, _) ->
            remove_invocation(Id, any)
        end,
    lists:foldl(Delete, ok, ListOfIds),
    ok;
handle_find_ids_result(Error) ->
    lager:error("deleting of invocations caused error: ~p", [Error]),
    ok.

dump_all() ->
    Print = fun(Entry, _) ->
                    lager:debug("~p", [Entry]),
                    ok
            end,
    Transaction = fun() ->
                          lager:debug("*** invocations ***"),
                          mnesia:foldl(Print, ok, ctrd_invocation),
                          lager:debug("*** end ***"),
                          ok
                  end,
    mnesia:transaction(Transaction).

create_table(DataConf) ->
    MnesiaDir = ctr_data_conf:mnesia_dir(DataConf),
    ct_data_util:create_mnesia_schema_if_needed(MnesiaDir),
    mnesia:delete_table(ctrd_invocation),
    InvDef = [{attributes, record_info(fields, ctrd_invocation)},
              {ram_copies, [node()]},
              {index, [realm, ts, caller_sess_id, caller_req_id]}
             ],
    {atomic, ok} = mnesia:create_table(ctrd_invocation, InvDef),
    ok.
